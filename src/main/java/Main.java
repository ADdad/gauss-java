public class Main {

    public static void main(String[] args) {
        double[][] matrix = {{5, 3, 1, 1, 0, 0}, {2, -1, 4, 0, 1, 0}, {3, 4, 9, 0, 0, 1}};
        printMatrix(matrix);
        System.out.println("===================");
        //not rounded
        double[][] gaus = Gauss.gauss(matrix);
        printMatrix(gaus);
    }

    private static void printMatrix(double[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            StringBuilder row = new StringBuilder();
            for (int j = 0; j < matrix[i].length; j++) {
                row.append(matrix[i][j]).append(", ");
            }
            System.out.println(row);
        }
    }
}
