public class Gauss {


    public static double[][] gauss(double[][] startMatrix) {
        boolean isResActual = false;
        double[][] firstMatrix = copyMatrix(startMatrix);
        double[][] resMatrix = new double[startMatrix.length][startMatrix[0].length];
        for (int diag = 0; diag < Math.min(firstMatrix[0].length, firstMatrix.length); diag++) {
            if (isResActual) {
                matrixIteration(resMatrix, firstMatrix, diag);
            } else {
                matrixIteration(firstMatrix, resMatrix, diag);
            }
            isResActual = !isResActual;
        }
        return isResActual ? resMatrix : firstMatrix;
    }

    private static void matrixIteration(double[][] firstMatrix, double[][] resMatrix, int diag) {
        double re = firstMatrix[diag][diag];
        for (int row = 0; row < firstMatrix.length; row++) {
            for (int column = 0; column < firstMatrix[0].length; column++) {
                if (row == diag) {
                    resMatrix[row][column] = firstMatrix[row][column] / re;
                    continue;
                }
                double a = firstMatrix[diag][column];
                double b = firstMatrix[row][diag];
                double se = firstMatrix[row][column];
                resMatrix[row][column] = calcElem(se, a, b, re);
            }
        }
    }

    private static double calcElem(double se, double a, double b, double re) {
        return se - (a * b) / re;
    }

    private static double[][] copyMatrix(double[][] original) {
        double[][] copy = new double[original.length][original[0].length];
        for (int i = 0; i < original.length; i++) {
            System.arraycopy(original[i], 0, copy[i], 0, original[0].length);
        }
        return copy;
    }

}
